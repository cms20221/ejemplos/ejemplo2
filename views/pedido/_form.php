<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total')->textInput([
        "placeholder" => "Introduce el total del pedido"
    ]) ?>

    <?= $form
            ->field($model, 'fecha')
            ->input("date")
    ?>

    <?= $form
            ->field($model, 'id_cliente')
             ->dropDownList($model->clientes(),[
                 "prompt" => "selecciona un cliente"
                 
                 
             ])
            
            
            ?>

    <?= $form
            ->field($model, 'id_comercial')
            ->listBox($model->comerciales())
            
     ?>

    <div class="form-group">
        <?= Html::submitButton('+', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
