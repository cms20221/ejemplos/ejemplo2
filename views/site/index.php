<?php

/* @var $this yii\web\View */

$this->title = 'Gestion de Pedidos de Ramon :)';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Hello!</h1>

        <p class="lead">aplicacion para la gestion de clientes....</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">empezamos con Yii!!</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Pedido</h2>

                <p>Tabla para gestionar los pedidos y las fechas de entregas</p>

                <p><a class="btn btn-outline-secondary" href="http://localhost/ejemplo2/web/index.php/pedido/index">crear pedido &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>clientes</h2>

                <p>Tabla para la gestion de los clientes :)</p>

                <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Comerciales</h2>

                <p>Tabla para gestionar sus visitas</p>

                <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
